// Angular
import { Component, OnInit, ViewChild } from "@angular/core";

import { AlertService } from "../../../core/apps/alert.service";
import { SuggestionService } from "../../../core/apps/suggestion.service";
import { AnnoncesService } from "../../../core/apps/annonces.service";
@Component({
	selector: "kt-dashboard",
	templateUrl: "./dashboard.component.html",
	styleUrls: ["dashboard.component.scss"],
})
export class DashboardComponent implements OnInit {
	TotalSignalement = [];
	TotalCorrespan = [];
	Totalanonce = [];
	constructor(
		private alertService: AlertService,
		private suggestionService: SuggestionService,
		private annocesService: AnnoncesService
	) {
		let alert = this.alertService.getListAlert().subscribe((actions) => {
			//this.TotalSignalement = [];
			return actions.map((a) => {
				this.TotalSignalement.push(a);
			});
		});
		let annonce = this.annocesService
			.getListAnnonces()
			.subscribe((actions) => {
				//this.TotalSignalement = [];
				return actions.map((a) => {
					this.Totalanonce.push(a);
				});
			});
		let sug = this.suggestionService
			.getListSuggestions()
			.subscribe((actions) => {
				//this.TotalSignalement = [];
				return actions.map((a) => {
					this.TotalCorrespan.push(a);
				});
			});
	}

	ngOnInit(): void {}
}
