import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificattionsComponent } from './notificattions.component';

describe('NotificattionsComponent', () => {
  let component: NotificattionsComponent;
  let fixture: ComponentFixture<NotificattionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificattionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificattionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
