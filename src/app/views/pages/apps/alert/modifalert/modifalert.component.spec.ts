import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifalertComponent } from './modifalert.component';

describe('ModifalertComponent', () => {
  let component: ModifalertComponent;
  let fixture: ComponentFixture<ModifalertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifalertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifalertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
