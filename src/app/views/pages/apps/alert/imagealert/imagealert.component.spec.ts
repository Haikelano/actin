import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagealertComponent } from './imagealert.component';

describe('ImagealertComponent', () => {
  let component: ImagealertComponent;
  let fixture: ComponentFixture<ImagealertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagealertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagealertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
