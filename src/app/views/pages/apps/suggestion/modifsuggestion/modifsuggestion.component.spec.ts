import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifsuggestionComponent } from './modifsuggestion.component';

describe('ModifsuggestionComponent', () => {
  let component: ModifsuggestionComponent;
  let fixture: ComponentFixture<ModifsuggestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifsuggestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifsuggestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
