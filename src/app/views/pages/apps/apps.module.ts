import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnnoncesComponent } from './annonces/annonces.component';
import {RouterModule, Routes} from '@angular/router';
import { NotificattionsComponent } from './notificattions/notificattions.component';
import {PartialsModule} from '../../partials/partials.module';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

// Material
import {
	MatInputModule,
	MatPaginatorModule,
	MatProgressSpinnerModule,
	MatSortModule,
	MatTableModule,
	MatSelectModule,
	MatMenuModule,
	MatProgressBarModule,
	MatButtonModule,
	MatCheckboxModule,
	MatDialogModule,
	MatTabsModule,
	MatNativeDateModule,
	MatCardModule,
	MatRadioModule,
	MatIconModule,
	MatDatepickerModule,
	MatExpansionModule,
	MatAutocompleteModule,
	MAT_DIALOG_DEFAULT_OPTIONS,
	MatSnackBarModule,
	MatTooltipModule, MatToolbarModule, MatFormFieldModule
} from '@angular/material';
import { SuggestionComponent } from './suggestion/suggestion.component';
import { AlertComponent } from './alert/alert.component';
import { DialogComponent } from './dialog/dialog.component';
import { AddAnnonceComponent } from './annonces/add-annonce/add-annonce.component';
import { SuggestionDialogComponent } from './suggestion/suggestion-dialog/suggestion-dialog.component';
import { MentionlegalComponent } from './mentionlegal/mentionlegal.component';
import { ModifierAnnonceComponent } from './annonces/modifier-annonce/modifier-annonce.component';

import { AddnotificationComponent } from './notificattions/addnotification/addnotification.component';
import { ModifsuggestionComponent } from './suggestion/modifsuggestion/modifsuggestion.component';
import { ModifalertComponent } from './alert/modifalert/modifalert.component';
import { ImagealertComponent } from './alert/imagealert/imagealert.component';
import { DocsComponent } from './docs/docs.component';
import { AdddocComponent } from './docs/adddoc/adddoc.component';
import { UpdatedocComponent } from './docs/updatedoc/updatedoc.component';
import { ViewdocComponent } from './docs/viewdoc/viewdoc.component';

const routes: Routes = [
	{path: 'annonces' , component: AnnoncesComponent},
	{path: 'notification' , component: NotificattionsComponent},
	{path: 'signalement' , component: AlertComponent},
	{path: 'correspondances' , component: SuggestionComponent},
	{path: 'mentionlegal' , component: MentionlegalComponent},
	{path: 'docs' , component: DocsComponent}
];
@NgModule({
  declarations: [
  	AnnoncesComponent,
	NotificattionsComponent,
	SuggestionComponent,
	AlertComponent,
	DialogComponent,
	AddAnnonceComponent,
	SuggestionDialogComponent,
	MentionlegalComponent,
	ModifierAnnonceComponent,
	AddnotificationComponent,
	ModifsuggestionComponent,
	ModifalertComponent,
	ImagealertComponent,
	DocsComponent,
	AdddocComponent,
	UpdatedocComponent,
	ViewdocComponent

  ],
  imports: [
	  CommonModule,
	  HttpClientModule,
	  PartialsModule,
	  FormsModule,
	  ReactiveFormsModule,
	  TranslateModule.forChild(),
	  MatButtonModule,
	  MatMenuModule,
	  MatSelectModule,
	  MatInputModule,
	  MatTableModule,
	  MatAutocompleteModule,
	  MatRadioModule,
	  MatIconModule,
	  MatNativeDateModule,
	  MatProgressBarModule,
	  MatDatepickerModule,
	  MatCardModule,
	  MatPaginatorModule,
	  MatSortModule,
	  MatCheckboxModule,
	  MatProgressSpinnerModule,
	  MatSnackBarModule,
	  MatExpansionModule,
	  MatTabsModule,
	  MatTooltipModule,
	  MatDialogModule,
	  MatToolbarModule,
	  MatButtonModule,
	  MatFormFieldModule,
	  MatInputModule,
     RouterModule.forChild(routes),
  ]
})

export class AppsModule { }
