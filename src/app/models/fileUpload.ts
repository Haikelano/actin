export class FileUpload {
 
    key: string;
    name: string;
    url: string;
    file: File;
	payload: any;
   
    constructor(file: File) {
      this.file = file;
    }
  }