export class Docs {
	id: string;
	doc_name: string;
	doc_type: string;
	doc_url: string;
	city: string;

	constructor(
		doc_name: string,
		doc_type: string,
		doc_url: string,
		city: string
	) {
		this.doc_name = doc_name;
		this.doc_type = doc_type;
		this.doc_url = doc_url;
		this.city = city;
	}
}
