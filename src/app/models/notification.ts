import { Timestamp } from "rxjs/internal/operators/timestamp";

export class Notification {
	title: string;
	body: string;
	time: number;
	constructor(title: string, body: string, time: number) {
		this.title = title;
		this.body = body;
		this.time = time;
	}
}
